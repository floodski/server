#!usr/bin/python3
import os, os.path
import cherrypy
import random
import string
import sqlite3

DB_STRING = "secret.db"

listen_ip = "0.0.0.0"
listen_port = 10010

class MainApp(object):
    @cherrypy.expose
    def default(self):
        page = "404 Unknown request"
        cherrypy.response.status = 404
        return page

    @cherrypy.expose
    def index(self):
        #page = '<head><link href="/static/style.css" rel="stylesheet"></head>' + "<h1>### Hey mate ###</h1>" + "</br>"
        page = '<head><link href="/static/templated-snapshot/assets/css/main.css" rel="stylesheet"></head>' + "<h1>### Hey mate ###</h1>" + "</br>"
        try:
            page += "<body>Good to see you " + cherrypy.session['username'] + "!<br/>"
            page += "Enjoy yourself<br><br>"
            page += '<form action="/profile" method="post" enctype="multipart/form-data">'
            page += '<input type="submit" value="Profile"/></form>'
            page += '<form action="/getOnlineUserList" method="post" enctype="multipart/form-data">'
            page += '<input type="submit" value="Who is online?"/></form>'
            page += '<form action="/composeMessage" method="post" enctype="multipart/form-data">'
            page += '<input type="submit" value="Send a message"/></form>'
            page += '<form action="/readMessages" method="post" enctype="multipart/form-data">'
            page += '<input type="submit" value="Read Messages"/></form>'
            page += '<form action="/signout" method="post" enctype="multipart/form-data">'
            page += '<input type="submit" value="Log Out"/></form></body>'
        except KeyError: #There is no username
            page += "<br><br>Click here to <a href='login'>login</a>."

        return page

    @cherrypy.expose
    def login(self):
        page = '<form action="/APILogin" method="post" enctype="multipart/form-data">'
        page += 'Username: <input type="text" name="username"/><br/>'
        page += 'Password: <input type="password" name="password"/>'
        page += '<input type="submit" value="Login"/></form>'
        return page

    @cherrypy.expose
    def APILogin(self, username, password):
        page = 'Authenticating...'
        cherrypy.session['username'] = username
        raise cherrypy.HTTPRedirect('/')

    @cherrypy.expose
    def generate(self, length=8):
        return ''.join(random.sample(string.hexdigits, int(length)))


def runMainApp():
    # Create an instance of MainApp and tell Cherrypy to send all requests under / to it. (ie all of them)
    cherrypy.tree.mount(MainApp(), "/", conf)

    cherrypy.config.update(conf)

    print ("========================================")
    print ("Helios Server")
    print ("========================================")
    print ("Initialising...")

    # Start the web server
    cherrypy.engine.start()

    # And stop doing anything else. Let the web server take over.
    cherrypy.engine.block()

if __name__ == '__main__':
    conf = {
        '/': {
            'tools.sessions.on': True,
            'tools.staticdir.root': os.path.abspath(os.getcwd())
        },
        '/static': {
            'tools.staticdir.on': True,
            'tools.staticdir.dir': './public'
        }
    }
#Run the function to start everything
    runMainApp()
